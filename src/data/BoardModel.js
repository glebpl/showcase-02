import EventEmitter from './EventEmitter';
import { findNoteIndex } from '../utils';
import NoteModel from './NoteModel';

export default class BoardModel extends EventEmitter {
    constructor() {
        super();

        /**
         * @type {NoteModel[]}
         * @private
         */
        this._notes = [];
    }

    /**
     * @return {Map<string, NoteModel>}
     */
    get notes() {
        return this._notes;
    }

    createNote(options) {
        const note = new NoteModel({
            ...options
            , id: this.notes.length
        });
        this.notes.push(note);
        this.emit('update');
        return note;
    }

    findNoteById(noteId) {
        return this.notes[findNoteIndex(this.notes, noteId)];
    }

    moveNoteToFront(note) {
        const nn = this.notes;
        if(nn.length > 1) {
            const index = findNoteIndex(nn, note.id);
            if(index < nn.length - 1) {
                nn.splice(index, 1);
                nn.push(note);
                this.emit('update');
            }
        }
    }

    /**
     * @param {string} noteId
     */
    deleteNote(noteId) {
        const index = findNoteIndex(this.notes, noteId);
        this.notes.splice(index, 1);
        this.emit('update');
    }

    persist() {
        console.log('PERSIST');
        localStorage.setItem('sticky', JSON.stringify(this.toJson()));
    }

    restore() {
        const stored = localStorage.getItem('sticky');
        try {
            this._notes = JSON.parse(stored).map(data => new NoteModel(data));
        } catch(e) {}
    }

    /**
     * Serializes notes for storage or backend
     * @return {any[]}
     */
    toJson() {
        return this.notes.map(note => note.toJson());
    }
}
