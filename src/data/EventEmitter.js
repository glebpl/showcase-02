import { without } from '../utils';

export default class EventEmitter {
    constructor() {
        this._listeners = new Map();
    }

    on(event, listener) {
        const map = this._listeners;
        const ll = map.get(event) || [];
        ll.push(listener);
        map.set(event, ll);
    }

    off(event, listener) {
        const map = this._listeners;
        if(map.has(event)) {
            map.set(event, without(map.get(event), listener));
        }
    }

    emit(event, ...args) {
        const map = this._listeners;
        if(map.has(event)) {
            map.get(event).forEach(listener => {
                listener(...args);
            });
        }
    }
}
