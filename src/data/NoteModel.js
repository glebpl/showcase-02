import EventEmitter from './EventEmitter';
import { DEFAULT_NOTE_SIZE } from '../constants';

export default class NoteModel extends EventEmitter {
    constructor(options) {
        super();
        const {
            id
            , backgroundColor = '#f5f5c4'
            , position = {x: 16, y: 16}
            , size = {...DEFAULT_NOTE_SIZE}
            , text
        } = options;

        this._id = id;
        this._backgroundColor = backgroundColor;
        this._position = position;
        this._size = size;
        this._text = text;
    }

    get id() {
        return this._id;
    }

    get backgroundColor() {
        return this._backgroundColor;
    }

    set backgroundColor(value) {
        this._backgroundColor = value;
        this.emit('update');
    }

    get position() {
        return this._position;
    }

    set position(value) {
        this._position = value;
        this.emit('update');
    }

    get size() {
        return this._size;
    }

    set size(value) {
        this._size = value;
        this.emit('update');
    }

    get text() {
        return this._text;
    }

    set text(value) {
        this._text = value;
        this.emit('update');
    }

    toJson() {
        return {
            backgroundColor: this.backgroundColor
            , id: this.id
            , position: this.position
            , size: this.size
            , text: this.text
        };
    }
}



