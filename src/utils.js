import { COLORS } from './constants';
import { useReducer } from 'react';

export const cssTranslate = ({x, y}) => `translate3d(${x}px, ${y}px, 0)`;

export const getViewport = () => ({
    w: document.body.clientWidth
    , h: document.body.scrollWidth
});

export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const without = (arr, ...els) => arr.filter(a => els.indexOf(a) < 0);

const colorGenerator = (function* colorGenerator() {
    let i = 0;
    while(++i) {
        yield COLORS[(i - 1) % COLORS.length];
    }
})();

/*
variants
export const getRandomColor = () => {
    return `hsl(${getRandomInt(0, 255)}, ${getRandomInt(70, 85)}%, ${getRandomInt(88, 95)}%)`;
};*/
// export const getRandomColor = () => COLORS[getRandomInt(0, COLORS.length - 1)];
export const getNextColor = () => colorGenerator.next().value;

export const toggleClass = (el, className, on) => {
    el.classList[on ? 'add' : 'remove'](className);
};

export const useForceRender = () => useReducer(n => n + 1, 0)[1];

export const findNoteIndex = (notes, noteId) => notes.findIndex(n => n.id === noteId);

export const preventDefault = e => e.preventDefault();

export const normalizePosition = (x, y, size) => {
    let {w, h} = size;
    const {w: vw, h: vh} = getViewport();
    x = Math.min(Math.max(0, x), vw - w);
    y = Math.min(Math.max(0, y), vh - h);
    return {x, y};
};

export const throttle = (func, ms) => {

    let isThrottled = false,
        savedArgs,
        savedThis;

    function wrapper() {

        if (isThrottled) { // (2)
            savedArgs = arguments;
            savedThis = this;
            return;
        }

        func.apply(this, arguments); // (1)

        isThrottled = true;

        setTimeout(function() {
            isThrottled = false; // (3)
            if (savedArgs) {
                wrapper.apply(savedThis, savedArgs);
                savedArgs = savedThis = null;
            }
        }, ms);
    }

    return wrapper;
};
