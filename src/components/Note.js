import React, { useRef, useCallback, useEffect, useContext } from 'react';
import { cssTranslate, preventDefault, throttle, toggleClass, useForceRender } from '../utils';
import BoardContext from './BoardContext';

const Note = props => {
    const board = useContext(BoardContext);

    const {
        model
        , onFocus
        , zIndex
        , ...rest
    } = props;

    const {
        id
        , backgroundColor
        , position
        , text
        , size
    } = model;

    const ref = useRef(null);
    const forceRender = useForceRender();

    useEffect(() => {
       const handleUpdate = () => {
           board.persist();
           forceRender();
       };
       model.on('update', handleUpdate);
       return () => {
           model.off('update', handleUpdate);
       }
    }, [board, forceRender, model]);

    useEffect(() => {
        const observer = new MutationObserver(throttle((mutations) => {
            mutations.forEach(function(mutation) {
                if (mutation.type === 'attributes') {
                    const {w, h} = model.size;
                    const {width, height} = ref.current.getBoundingClientRect();
                    if(w !== width || h !== height) {
                        model.size = {w: width, h: height};
                    }
                }
            });
        }, 200));

        observer.observe(ref.current, {
            attributes: true
        });
    }, [forceRender, model]);

    const handleDragStart = useCallback(e => {
        toggleClass(ref.current, 'dragged', true);
        const {left, top} = ref.current.getBoundingClientRect();
        e.dataTransfer.setData('text/plain', JSON.stringify({
            id
            , startOffset: {
                left: e.clientX - left
                , top: e.clientY - top
            }
        }));
    }, [id]);

    const handleDragEnd = useCallback(e => {
        toggleClass(ref.current, 'dragged', false);
    }, []);

    const handleFocus = useCallback(() => {
        board.moveNoteToFront(model);
    }, [board, model]);

    const handleChange = useCallback(e => {
        model.text = e.target.value
    }, [model]);

    return (
        <textarea
            ref={ref}
            autoFocus
            className="note"
            draggable
            maxLength={2048}
            onChange={handleChange}
            onFocus={handleFocus}
            onDragStart={handleDragStart}
            onDragEnd={handleDragEnd}
            onDragOver={preventDefault}
            style={{
                backgroundColor
                , transform: cssTranslate(position)
                , width: `${size.w}px`
                , height: `${size.h}px`
                , zIndex
            }}
            value={text}
            {...rest}
        />
    );
};

export default Note;
