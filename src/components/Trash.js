import React, { useCallback, useRef, useEffect, useContext } from 'react';
import { preventDefault, toggleClass } from '../utils';
import BoardContext from './BoardContext';

const Trash = () => {
    const board = useContext(BoardContext);
    const ref = useRef(null);

    const handleDragEnter = useCallback((e) => {
        toggleClass(ref.current, 'drag-over', true);
        // e.dataTransfer.dropEffect = 'move';
    }, []);

    const handleDragLeave = useCallback((e) => {
        toggleClass(ref.current, 'drag-over', false);
    }, []);

    useEffect(() => {
        const handleDrop = e => {
            preventDefault(e);
            if(e.target === ref.current) {
                const data = JSON.parse(e.dataTransfer.getData('text/plain'));
                board.deleteNote( data.id);
                toggleClass(ref.current, 'drag-over', false);
            }
        };
        document.addEventListener('drop', handleDrop, false);
        return () => {
            document.removeEventListener('drop', handleDrop);
        };
    }, [board]);

    return (
        <div
            ref={ref}
            className="trash"
            onDragOver={preventDefault}
            onDragEnter={handleDragEnter}
            onDragLeave={handleDragLeave}
        >
            Drop here for recycling
        </div>
    );
};

export default Trash;
