import React, { useEffect, useCallback, useContext, useRef } from 'react';
import Note from './Note';
import { DEFAULT_NOTE_SIZE } from '../constants';
import {
    getNextColor,
    normalizePosition,
    preventDefault,
    useForceRender
} from '../utils';
import BoardContext from './BoardContext';

const Board = () => {
    const board = useContext(BoardContext);
    const forceRender = useForceRender();
    const ref = useRef(null);

    useEffect(() => {
        const handleUpdate = () => {
            board.persist();
            forceRender();
        };
        board.on('update', handleUpdate);
        return () => {
            board.off('update', handleUpdate);
        }
    }, [board, forceRender]);

    useEffect(() => {
        const handleDrop = e => {
            preventDefault(e);
            if(ref.current.contains(e.target)) {
                const data = JSON.parse(e.dataTransfer.getData('text/plain'));
                const {id, startOffset} = data;
                const note = board.findNoteById(id);
                if(note) {
                    note.position = normalizePosition(e.clientX - startOffset.left, e.clientY - startOffset.top, note.size);
                }
            }
        };
        document.addEventListener('drop', handleDrop, false);
        return () => {
            document.removeEventListener('drop', handleDrop);
        };
    }, [board]);

    const handleDoubleClick = useCallback(e => {
        if(e.target === ref.current) {
            preventDefault(e);
            board.createNote({
                backgroundColor: getNextColor()
                , position: normalizePosition(e.clientX, e.clientY, DEFAULT_NOTE_SIZE)
            });
        }
    }, [board]);

    const notes = board.notes;

    return (
        <div
            ref={ref}
            className="board"
            onDoubleClick={handleDoubleClick}
            onDragOver={preventDefault}
        >
            {notes.map((note, index) => (
                <Note
                    key={note.id}
                    model={note}
                    zIndex={index}
                />
            ))}
        </div>
    );
};

export default Board;
