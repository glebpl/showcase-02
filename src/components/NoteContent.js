import React from 'react';

const NoteContent = props => {
    return (
        <textarea className="note-content" {...props} />
    );
};

export default NoteContent;
