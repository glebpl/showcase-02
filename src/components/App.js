import React, { useMemo } from 'react';
import '../app.css';
import Board from './Board';
import Trash from './Trash';
import BoardModel from '../data/BoardModel';
import BoardContext from './BoardContext';

const App = () => {
    const board = useMemo(() => {
        const board = new BoardModel();
        board.restore();
        if(!board.notes.length) {
            board.createNote({
                text: 'I\'m your first note. Double click to add other.'
            });
        }
        return board;
    }, []);

    return (
        <div className="app">
            <BoardContext.Provider value={board}>
                <Board />
                <Trash />
            </BoardContext.Provider>
        </div>
    );
};

export default App;
