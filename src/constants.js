export const DEFAULT_NOTE_SIZE = {
    w: 160, h: 120
};

export const COLORS = [
    '#f5d0c9'
    , '#f5e3cb'
    , '#f5f5c4'
    , '#e4f5c4'
    , '#d9f5cb'
    , '#cbf5e0'
    , '#cbe7f5'
    , '#cbd9f5'
    , '#d2cbf5'
    , '#e7cbf5'
];
